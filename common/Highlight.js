/*jshint esversion: 9 */
/*
 代码高亮
 */
import hljs from 'highlight.js';  //可以cdn引入
import 'highlight.js/styles/github-gist.css';
function highlightCode(){  //使代码高亮的方法
    const block = document.querySelectorAll('pre code');
    //hljs.configure({useBR: true});  //取消换行
    block.forEach((el) => {
        hljs.highlightBlock(el);
    });
}

export default{
    highlightCode,
};
