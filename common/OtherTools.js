/*jshint esversion: 9 */
/*
 小工具
 */

 export function removeSpace(data) {  //字符串去除空格
    if (data) {
        data = data.replace(/\s+/g, ""); //去空格
    }
    return data;
}
/**
 * 格式化文件大小, 输出成带单位的字符串
 * @method formatFileSize
 * @grammar Base.formatFileSize( size ) => String
 * @grammar Base.formatFileSize( size, pointLength ) => String
 * @grammar Base.formatFileSize( size, pointLength, units ) => String
 * @param {Number} size 文件大小
 * @param {Number} [pointLength=2] 精确到的小数点数。
 * @param {Array} [units=[ 'B', 'K', 'M', 'G', 'TB' ]] 单位数组。从字节，到千字节，一直往上指定。如果单位数组里面只指定了到了K(千字节)，同时文件大小大于M, 此方法的输出将还是显示成多少K.
 * @example
 * console.log( Base.formatFileSize( 100 ) );    // => 100B
 * console.log( Base.formatFileSize( 1024 ) );    // => 1.00K
 * console.log( Base.formatFileSize( 1024, 0 ) );    // => 1K
 * console.log( Base.formatFileSize( 1024 * 1024 ) );    // => 1.00M
 * console.log( Base.formatFileSize( 1024 * 1024 * 1024 ) );    // => 1.00G
 * console.log( Base.formatFileSize( 1024 * 1024 * 1024, 0, ['B', 'KB', 'MB'] ) );    // => 1024MB
*/
export function formatFileSize( size, pointLength, units ) {
    var unit;
    units = units || [ 'B', 'K', 'M', 'G', 'TB' ];
    while ( (unit = units.shift()) && size > 1024 ) {
        size = size / 1024;
    }
    return (unit === 'B' ? size : size.toFixed( pointLength || 2 )) + unit;
}
export function isPreviewFile(fileName) { //是否是可预览文件
    if (!fileName) return false;
    let types = fileName.split('.');
    let type = types[types.length - 1];
    return 'png|jpg|pdf'.indexOf(type) !== -1;
}
export function isPhone(str) { //是电话号码
    const reg = /^1[0-9][0-9]\d{8}$/;
    return reg.test(str);
}
export function getFileType(fileName) { //获取文件类型
    if (!fileName) return fileName;
    let types = fileName.split('.');
    return types[types.length - 1];
}
export function flattenTwoList(list = []) { //将二维数组展开
    let dataList = [];
    for (let item of list) {
        dataList.push(...item);
    }
    return dataList;
}
export function getIcon(link) { //根据链接获取网站图标
    if (!link) return link;
    let urlReg = /[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/;
    let _list = urlReg.exec(link);
    if (_list && _list.length > 0) {
        return "https://" + _list[0] + "/favicon.ico";
    } else {
        return link;
    }
}
export function doubleClick(callBack) { //双击事件
    let time = 0;
    return function () {
        let now = new Date().getTime();
        if ((now - time) < 300) { //表示是双击事件
            callBack.call(this, ...arguments);
        }
        time = now;
    };
}
export function isEmail(s) { //是否是邮箱
    return /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s);
}
export function randomString(e) {  //随机获取相应位数的字符串
    e = e || 32;
    let t = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678",
    a = t.length,
    n = "";
    for (let i = 0; i < e; i++) n += t.charAt(Math.floor(Math.random() * a));
    return n;
}
export function highlightFilter(text,hlText){  //高亮文本过滤器
    if(!text || !hlText) return text;
    return text.replace(new RegExp(hlText,'g'),`<span>${hlText}</span>`);
}
export function isWeiXin(){  //判断是否是微信浏览器
    let ua = navigator.userAgent.toLowerCase();  
    if(ua.match(/MicroMessenger/i)=="micromessenger") {  
        return true;  
    } else {  
        return false;  
    }
}
export function delHtmlTag(str){  //字符串去掉所有的html标记
    if(!str) return str;
    return str.replace(/<[^>]+>/g,"");
}
export function createObjectURL(file) {  //创建文件路径
    var url = null;
    if (window.createObjectURL !== undefined) { // basic
        url = window.createObjectURL(file);
    } else if (window.URL !== undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file);
    } else if (window.webkitURL !== undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}
export function isURL(url){  //检查是否是一个完整的URL
    var exp=new RegExp(/http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\\/?%&=]*)?/);
    return exp.test(url);
}
// export function test(list){  //判断数组里数字正负是否统一
//     let a = 0;  //和
//     let b = 0;  //绝对值和
//     list.forEach(item=>{
//         if(typeof item != 'number' || Number.isNaN(item)) return;
//         a += item;
//         b += Math.abs(item);
//     });
//     return Math.abs(a) == Math.abs(b);
// }
export function randomNum(minNum,maxNum){  //生成从minNum到maxNum的随机数
    switch(arguments.length){ 
        case 1: 
            return parseInt(Math.random()*minNum+1,10); 
        case 2: 
            return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10); 
        default: 
            return 0; 
    } 
}
export function setIcon(href){  //写入icon
    let icon = document.querySelector("link[rel='shortcut icon'],link[rel='icon']");
    if(icon){
        icon.href = href;
    }else{
        icon = document.createElement('link');
        icon.type = 'image/x-icon';
        icon.rel = 'shortcut icon';
        icon.href = href;
        document.getElementsByTagName('head')[0].appendChild(icon);
    }
    return icon;
}
export function isClient(){  //是客户端
    return process.client;
}
export function isProdoction(){  //是否是生产环境
    return process.env.NODE_ENV === "production";
}