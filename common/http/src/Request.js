/*jshint esversion: 9 */
/*
 http请求配置
 */
import axios from "axios";
import Const from "./Const";

export const service = axios.create({  //可创建多个 axios实例
    baseURL: Const.baseApiURL, //设置公共的请求前缀
    timeout: Const.timeout, //超时终止请求
});

service.interceptors.request.use(
    config => {
        config.headers.token = '';
        return config;
    },
    error => Promise.error(error),
);

service.interceptors.response.use(
    response => {
        const data = response.data;
        const state = data.state;
        switch (state) {
            case 200:
                return data;
            case 201: //表示登陆失效
                return Promise.reject(data);
            default:
                return Promise.reject(data);
        }
    },
    () => { //数据请求发生错误
        return Promise.reject({
            msg: '请求发生错误',
        });
    },
);