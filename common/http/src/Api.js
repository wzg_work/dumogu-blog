/*jshint esversion: 9 */
import {
    service,
} from "./Request";
import {
    formatURLSearchParams,
} from "./Tools";
/*
 登陆管理
 */
function login({headers,params,data,onUploadProgress}={}){  //用户登陆，实现了直接调用即可
    return service({
        url:"/app0/login/login",
        method:'post',
        headers:headers,
        params:formatURLSearchParams(params),
        data:data,
        onUploadProgress:onUploadProgress,
    });
}
const login2 = {  //只提供接口信息，不管实现方法
    url:'/app0/login/login',
    method:'post',
};
const getUser = "";  //获取用户接口，变量
export default{
    login,
    login2,
    getUser,
};