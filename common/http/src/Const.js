/*jshint esversion: 9 */
import {isProdoction} from './Tools';

const TOKENKEY = "ssid";  //上传token时对应的键名
const timeout = 1000;  //api请求超时时间
let baseImgUrl;  //图片资源的链接
let baseApiURL;  //api原始链接
if(isProdoction()){  //如果是生产环境
    baseImgUrl = 'https://jqllxew-1256599956.cos.ap-chengdu.myqcloud.com/';
    baseApiURL = 'http://192.168.0.25:8081';
}else{
    baseImgUrl = 'https://zcb-1258966087.cos.ap-chengdu.myqcloud.com/';
    baseApiURL = '/api';
}
export default{
    TOKENKEY,
    baseImgUrl,
    baseApiURL,
    timeout,
};