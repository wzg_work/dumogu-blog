/*jshint esversion: 9 */
export default {
    mode: 'universal',
    head: {
        title: "毒蘑菇博客",
        meta: [
            {
                charset: 'utf-8'
            },
            {
                'http-equiv': 'content-language',
                content: 'zh-cn'
            }, //设置网页语言
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: '不是只有视频和音乐才带给人欢乐！还有一篇文字哟！学生时代所看过的短文还有谁记得呢？甚是怀念呢！'
            },
            {
                name: 'sogou_site_verification',
                content: 'SqbjD8vhx3'
            },
            {
                name: '360-site-verification',
                content: '8646db7f8ae653c25b3ff7644cd2f310'
            },
            {
                name: 'msvalidate.01',
                content: 'C419C93C6C8567D38FC74B3371D63F5B'
            },
            {
                name: 'baidu-site-verification',
                content: 'kxg9aoQDFa'
            },
            {
                name: 'author',
                content: '2978968560@qq.com'
            },
            {
                name: 'og:title',
                content: '一篇文字'
            },
            {
                name: 'og:url',
                content: 'https://www.dumogu.top/'
            },
            {
                name: 'og:site_name',
                content: '一篇文字'
            },
            {
                name: 'og:description',
                content: '不是只有视频和音乐才带给人欢乐！还有一篇文字！'
            },
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }],
        script: [
            {
                type: 'text/javascript',
                data_body: "true",
                async: "true",
                src: 'https://cdn.bootcdn.net/ajax/libs/velocity/2.0.6/velocity.min.js',
            },
            {  //百度统计
                type: 'text/javascript',
                data_body: "true",
                innerHTML: 'var _hmt = _hmt || [];\n' +
                '(function() {' +
                'var hm = document.createElement("script");\n' +
                'hm.src = "https://hm.baidu.com/hm.js?c918670da50fb6603768af2cfca470a5";\n' +
                'var s = document.getElementsByTagName("script")[0]; \n' +
                's.parentNode.insertBefore(hm,s);\n' +
                '})();',
            },
        ],
        __dangerouslyDisableSanitizers: ['script'],
    },
    /*
    ** Global CSS
    */
    css: [
        'element-ui/lib/theme-chalk/index.css',
        '@/assets/main.css',
    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {
            src: '@/plugins/element-ui',
            ssr: true
        },
        {
            src: '@/plugins/index'
        },
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [],
    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/axios',
    ],
    // axios: {
    //   // proxyHeaders: false
    // },
    /*
    ** Build configuration
    */
    build: {
        vendor: ['element-ui'],
        babel: { //配置element按需加载
            plugins: [
                [
                    'component',
                    {
                        libraryName: 'element-ui',
                        styleLibraryName: 'theme-chalk'
                    }
                ],
            ],
            comments: true
        },
        loaders: {
            file: {},
            fontUrl: {
                limit: 1000
            },
            imgUrl: {
                limit: 10000
            },
            pugPlain: {},
            vue: {
                transformAssetUrls: {
                    video: 'src',
                    source: 'src',
                    object: 'src',
                    embed: 'src'
                }
            },
            css: {},
            cssModules: {
                localIdentName: '[local]_[hash:base64:5]'
            },
            less: {},
            sass: {
                indentedSyntax: true
            },
            scss: {},
            stylus: {},
            vueStyle: {}
        },
        // plugins: [
        //   new CompressionPlugin({
        //     minRatio: 0.8,
        //     test: /\.js$|\.html$|\.css/, // 匹配文件名
        //     threshold: 10240, // 对超过10kb的数据进行压缩
        //     deleteOriginalAssets: false // 是否删除原文件
        //   })
        // ],
        transpile: [/^element-ui/],
        extractCSS: {
            allChunks: true
        },
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {}
    }
};
