/*jshint esversion: 9 */
import Vue from 'vue';

import {
    Icon,//图标
    Pagination,  //分页
    Button,  //按钮
    Tag, //标签
    Image,  //图片容器
    Timeline,
    TimelineItem,
    Message,
} from 'element-ui';
Vue.use(Icon);
Vue.use(Pagination);
Vue.use(Button);
Vue.use(Tag);
Vue.use(Image);
Vue.use(Timeline);
Vue.use(TimelineItem);

Vue.prototype.$message = Message;
